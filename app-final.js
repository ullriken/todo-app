(function (global) {

    function App() {
        // Properties
        this.todos = [];
        this.newTodoTitle = '';
        // HTML Element References
        this.elTodoItems;
        this.elCompletedItems;
        this.elBtnAdd;
        this.elNewTodo;
    }

    App.prototype.init = function () {
        this.elTodoItems = document.getElementById('todo-item-list');
        this.elCompletedItems = document.getElementById('completed-item-list');
        this.elBtnAdd = document.getElementById('btn-add-todo');
        this.elNewTodo = document.getElementById('todo-title');
        // Event Handlers
        this.elBtnAdd.addEventListener('click', (e) => {
            if (this.newTodoTitle.length > 0) {
                this.addTodo();
            } else {
                alert('Please add a todo title first');
            }
        });
        this.elNewTodo.addEventListener('keyup', (e) => {
            this.newTodoTitle = e.target.value.trim();
            if (e.keyCode == 13) {
                this.elBtnAdd.click();
            }
        });

    }

    App.prototype.generateNewId = function () {
        let id = 1;
        if (this.todos.length > 0) {
            const numberOfItems = this.todos.length;
            id = this.todos[numberOfItems - 1].id + 1;
        }

        return id;
    }

    App.prototype.addTodo = function () {
        const todo = new Todo(this.generateNewId(), this.newTodoTitle);
        this.todos.push(todo);
        this.render();
        this.elNewTodo.value = '';
        this.newTodoTitle = '';
    };

    App.prototype.completeTodo = function (id) {
        const itemToComplete = this.todos.find(todo => todo.id == id);
        itemToComplete.completed = true;        
        this.render();
    }

    App.prototype.render = function () {
        // Reset the lists.
        this.elCompletedItems.innerHTML = '';
        this.elTodoItems.innerHTML = '';
        // Update the UI.
        const completed = this.todos.filter(todo => todo.completed);
        const incompleted = this.todos.filter(todo => !todo.completed);

        for (let todo of completed) {
            this.elCompletedItems.appendChild(this.renderListItem(todo));
        }

        for (let todo of incompleted) {
            this.elTodoItems.appendChild(this.renderListItem(todo));
        }

        if (completed.length === 0) {
            this.elCompletedItems.appendChild(this.renderListItem({ id: -1, title: '😴 No items completed yet...' }))
        }

        if (incompleted.length === 0) {
            this.elTodoItems.appendChild(this.renderListItem({ id: -1, title: '🤷‍♀️ Nothing todo! Maybe take a nap...' }));
        }
    }

    App.prototype.renderListItem = function (todo) {
        const elLi = document.createElement('li');
        elLi.className = "list-group-item d-flex justify-content-between";
        elLi.innerText = todo.title;
        elLi.id = todo.id;

        if (todo.id > 0 && todo.completed === false) {
            const elBtn = document.createElement('button');
            elBtn.innerHTML = '&check;';
            elBtn.className = 'btn btn-success btn-sm';
            elBtn.addEventListener('click', (e) => {
                this.completeTodo(todo.id);
            });
            elLi.appendChild(elBtn);
        }

        return elLi;
    }

    function Todo(id, title) {
        this.id = id;
        this.title = title;
        this.completed = false;
    }

    global.TodoApp = App;
})(window);

const app = new TodoApp();
app.init();
function App() {
  //variables
  this.todos = [];
  this.newTodoTitle;
  //HTML Elements
  this.elNewTodoInput;
  this.elAddTodoBtn;
  this.elTodoItems;
  this.elCompletedItems;
}
App.prototype.init = function() {
  //Getting our HTML Elements
  this.elNewTodoInput = document.getElementById("todo-title");
  this.elTodoItems = document.getElementById("todo-item-list");
  this.elCompletedItems = document.getElementById("completed-item-list");
  this.elAddTodoBtn = document.getElementById("btn-add-todo");
  this.elNewTodoInput.addEventListener("keyup", e => {
    this.newTodoTitle = event.target.value.trim();
    if (e.keyCode == 13) {
      this.elAddTodoBtn.click();
    }
  });
  this.elAddTodoBtn.addEventListener("click", () => {
    this.addTodo();
  });
  return this;
};

App.prototype.addTodo = function() {
  if (this.newTodoTitle.length > 0) {
    const newId = this.genereteId();
    this.todos.push(new Todo(newId, this.newTodoTitle));
    this.render();
  } else {
    alert("DONT! you scum!");
  }
};

App.prototype.compleTodo = function(id) {
  // .. We need the id, where is the id
  const todo = this.todos.find(todo => {
    return todo.id == id;
  });
  if (todo) {
    todo.completed = true;
  }
  return todo;
};

App.prototype.genereteId = function() {
  let id = 0;
  if (this.todos.length === 0) {
    id = 1;
  } else {
    id = this.todos[this.todos.length - 1].id + 1;
  }
  return id;
};

App.prototype.render = function() {
  // 1. Reset the state of the UI
  this.elNewTodoInput.value = "";
  this.newTodoTitle = "";
  this.elTodoItems.innerHTML = "";
  this.elCompletedItems.innerHTML = "";

  // 2. Render the items that are NOT completed
  const incompleteTotos = this.todos.filter(todo => !todo.completed);

  for (const todo of incompleteTotos) {
    this.elTodoItems.appendChild(this.renderTodoItem(todo));
  }

  if (incompleteTotos.length === 0) {
    this.elTodoItems.appendChild(
      this.renderTodoItem({ title: "🤷‍♀️ Nothing todo", completed: true }, true)
    );
  }

  // 3. Render items that ARE completed
  const completeTotos = this.todos.filter(todo => todo.completed);

  for (const todo of completeTotos) {
    this.elCompletedItems.appendChild(this.renderTodoItem(todo));
  }
  if (completeTotos.length === 0) {
    this.elCompletedItems.appendChild(
      this.renderTodoItem(
        { title: "👨‍✈️ Start working you scum!", completed: true },
        true
      )
    );
  }
};

App.prototype.renderTodoItem = function(todo, diableStrike = false) {
  // Create a todo html element
  // Attach an EventListerner if item is NOT completed
  const eltodo = document.createElement("li");
  eltodo.className = "list-group-item d-flex justify-content-between";
  eltodo.innerText = todo.title;

  if (todo.completed === false) {
    // Create a button to complete
    const elCompleBtn = document.createElement("button");
    elCompleBtn.innerHTML = "&check;";
    elCompleBtn.className = "btn btn-success btn-sm";
    // Attach click event listerner to button
    elCompleBtn.addEventListener("click", () => {
      this.compleTodo(todo.id);
      this.render();
    });

    eltodo.appendChild(elCompleBtn);
  } else if (todo.completed && !diableStrike) {
    eltodo.className += " completed";
  }
  return eltodo;
};

function Todo(id, title) {
  this.id = id;
  this.title = title;
  this.completed = false;
}

const app = new App();

// Chain because init() returns .this
app.init().render();
document.getElementById("date").innerHTML = new Date().toDateString();
